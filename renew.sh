#!/bin/bash
SECONDARY_NS_SSH_USER=$(<secondary_ns_ssh_user)
SECONDARY_NS_SSH_HOST=$(<secondary_ns_ssh_host)
SECONDARY_NS_SSH_PORT=$(<secondary_ns_ssh_port)
NS_SERVICE_NAME="bind9"

lexical_retcode () {
    if [[ $1 -eq 0 ]]; then
        echo "Success"
    else
        echo "FAILED. Code $1"
    fi
}
secondary_ns_cmd () {
    ssh -p$SECONDARY_NS_SSH_PORT $SECONDARY_NS_SSH_USER@$SECONDARY_NS_SSH_HOST "$1"
    return $?
}
disable_secondary_ns () {
    echo "Shutting down secondary NS"
    secondary_ns_cmd "sudo systemctl stop $NS_SERVICE_NAME"
    RETCODE=$?
    lexical_retcode $RETCODE
    return $RETCODE
}
enable_secondary_ns () {
    echo "Starting up secondary NS"
    secondary_ns_cmd "sudo systemctl start $NS_SERVICE_NAME"
    RETCODE=$?
    lexical_retcode $RETCODE
    return $RETCODE
}
exec_certbot () {
    echo "Running Certbot... "
    /etc/letsencrypt/renew-certs.sh
    CERTBOT_RETCODE=$?
    lexical_retcode $CERTBOT_RETCODE
    return $CERTBOT_RETCODE
}
exec_systemctl_reload () {
    echo "Reloading $1..."
    systemctl reload "$1"
    SYSTEMCTL_RELOAD_RETCODE=$?
    lexical_retcode $SYSTEMCTL_RELOAD_RETCODE
    return $SYSTEMCTL_RELOAD_RETCODE
}
set -e
set -o pipefail
disable_secondary_ns && exec_certbot && enable_secondary_ns && exec_systemctl_reload apache2 && exec_systemctl_reload dovecot && exec_systemctl_reload postfix || (enable_secondary_ns && false)
