# Certbot Renewal

## Features

* Renews Certbot TLS certificates
* Reloads Apache2, Dovecot and Postfix post-renew
* All done in systemd service and timer


## Install

Copy over the `./*.example` files to files named the same thing but without `.example`
extension, and feed the correct values to them. After that, install and enable the
systemd unit+timer as described below.

There is an example of the `renew-certs.sh` script which should exist in `/etc/letsencrypt/`
named `renew-certs.sh.example`. Please make sure this is implemented.

```
ln -s /PATH/TO/THIS/REPO/certbot-renew.service /etc/systemd/system/
ln -s /PATH/TO/THIS/REPO/certbot-renew.timer /etc/systemd/system/
systemctl daemon-reload
systemctl enable certbot-renew.service # not sure if required
systemctl enable certbot-renew.timer
systemctl start certbot-renew.timer
```

### Native timer

Please disable native timer if you wish to use this

```
systemctl disable certbot.timer
systemctl disable certbot.service # not sure if required
```


## Author

Axel Latvala

06.11.2018
